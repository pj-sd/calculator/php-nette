<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression;

use App\Model\Calculator\Exceptions\RuntimeException;
use App\Model\Calculator\Expression\Elements\ElementInterface;
use App\Model\Calculator\Expression\Elements\Numerical\Expression;
use App\Model\Calculator\Expression\Elements\Numerical\Number;
use App\Model\Calculator\Expression\Elements\Numerical\NumericalInterface;
use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\AdditionSubtractionInterface;
use App\Model\Calculator\Expression\Elements\Operators\MultiplicationDivisionInterface;
use App\Model\Calculator\Expression\Elements\Operators\OperatorInterface;
use LogicException;

class ExpressionCalculator
{
	public static function calculateExpression(Expression $expression): float
	{
		$elements = $expression->getElements();
		self::runMultiplicationDivision($elements);

		return self::runAdditionSubtraction($elements);
	}

	/**
	 * @param ElementInterface[] $elements
	 */
	private static function runMultiplicationDivision(array &$elements): void
	{
		/** @var ElementInterface[] $calculatedElements */
		$calculatedElements = [];

		/** @var Number|null $previousNumber */
		$previousNumber = null;

		/** @var OperatorInterface|MultiplicationDivisionInterface|null $operator */
		$operator = null;

		foreach ($elements as $element) {
			if ($element instanceof MultiplicationDivisionInterface) {
				if (!$previousNumber instanceof NumericalInterface) {
					throw new RuntimeException('A number missing before multiplication/division operator.');
				}

				$operator = $element;
			} elseif ($element instanceof NumericalInterface) {
				if ($operator instanceof MultiplicationDivisionInterface) {
					if (!$previousNumber instanceof NumericalInterface) {
						throw new LogicException('Unexpected state in multiplication/division.');
					}

					$expressionResult = BaseExpressionCalculator::calculateBaseExpression(
						$previousNumber,
						$operator,
						$element
					);

					$previousNumber = new Number($expressionResult);
					$operator = null;
				} else {
					$previousNumber = $element;
				}
			} elseif ($element instanceof OperatorInterface) {
				if ($previousNumber instanceof NumericalInterface) {
					$calculatedElements[] = $previousNumber;
					$previousNumber = null;
				}

				$calculatedElements[] = $element;
			} else {
				throw new LogicException('Unexpected state in multiplication/division.');
			}
		}

		if ($previousNumber instanceof NumericalInterface) {
			$calculatedElements[] = $previousNumber;
			$previousNumber = null;
		}

		$elements = $calculatedElements;
	}

	/**
	 * @param ElementInterface[] $elements
	 *
	 * @return float
	 */
	private static function runAdditionSubtraction(array $elements): float
	{
		$result = new Number(0.0);

		/** @var AdditionSubtractionInterface|null $operator */
		$operator = new Addition;

		foreach ($elements as $element) {
			if ($element instanceof AdditionSubtractionInterface) {
				$operator = $element;
			} elseif ($element instanceof NumericalInterface) {
				if (!$operator instanceof AdditionSubtractionInterface) {
					throw new LogicException('Unexpected state in addition/subtraction.');
				}

				$expressionResult = BaseExpressionCalculator::calculateBaseExpression(
					$result,
					$operator,
					$element
				);

				$result = new Number($expressionResult);
			} else {
				throw new LogicException('Unexpected state in addition/subtraction.');
			}
		}

		return $result->getValue();
	}
}
