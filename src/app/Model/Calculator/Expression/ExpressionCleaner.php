<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression;

class ExpressionCleaner
{
	public static function clearStringExpression(string $stringExpression): string
	{
		$stringExpression = str_replace(' ', '', $stringExpression);
		$stringExpression = str_replace(
			['+-', '--', '*-', '/-'],
			['-', '+', '*(-1)*', '/(-1)/'],
			$stringExpression
		);

		return $stringExpression;
	}
}
