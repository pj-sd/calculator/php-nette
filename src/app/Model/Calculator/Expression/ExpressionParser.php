<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression;

use App\Model\Calculator\Exceptions\ExpressionEndsWithOperatorException;
use App\Model\Calculator\Exceptions\InvalidCharactersException;
use App\Model\Calculator\Exceptions\InvalidDoubledOperatorsException;
use App\Model\Calculator\Exceptions\InvalidElementBeforeClosingBracketException;
use App\Model\Calculator\Exceptions\InvalidElementCharacterException;
use App\Model\Calculator\Exceptions\MissingClosingBracketException;
use App\Model\Calculator\Exceptions\MissingOpeningBracketException;
use App\Model\Calculator\Expression\Elements\Recognition\BracketRecognizer;
use App\Model\Calculator\Expression\Elements\Numerical\Expression;
use App\Model\Calculator\Expression\Elements\Recognition\NumberRecognizer;
use App\Model\Calculator\Expression\Elements\Numerical\Digit;
use App\Model\Calculator\Expression\Elements\Numerical\Number;
use App\Model\Calculator\Expression\Elements\Recognition\OperatorRecognizer;

class ExpressionParser
{
	public static function parseString(string $stringExpression): Expression
	{
		$stringExpression = ExpressionCleaner::clearStringExpression($stringExpression);

		self::checkExpressionDoesNotContainInvalidCharacters($stringExpression);
		self::checkExpressionDoesNotContainDoubledOperators($stringExpression);
		self::checkExpressionDoesNotEndWithOperator($stringExpression);

		$expression = new Expression;

		for ($i = 0; $i < strlen($stringExpression); $i++) {
			$character = $stringExpression[$i];

			switch (true) {
				case OperatorRecognizer::isOperatorSymbol($character):
					$expression->addOperator(OperatorRecognizer::recognizeOperator($character));
					break;
				case NumberRecognizer::isCharDigit($character):
					$expression->addDigit(new Digit((int) $character));
					break;
				case BracketRecognizer::isOpeningBracketSymbol($character):
					$expression = new Expression($expression);
					break;
				case BracketRecognizer::isClosingBracketSymbol($character):
					if (!$expression->hasParent()) {
						throw new MissingOpeningBracketException('At least one opening bracket is missing in the expression.');
					}

					self::checkElementBeforeClosingBracket($expression);

					$expression = $expression->getParent();
					break;
				default:
					throw new InvalidElementCharacterException(sprintf('Invalid element character %s.', $character));
			}
		}

		if ($expression->hasParent()) {
			throw new MissingClosingBracketException('At least one closing bracket is missing in the expression.');
		}

		return $expression;
	}

	private static function checkExpressionDoesNotContainInvalidCharacters(string $stringExpression): void
	{
		if (!preg_match('/^[()+\-\*\/0-9]*$/', $stringExpression)) {
			throw new InvalidCharactersException('The expression contains invalid characters.');
		}
	}

	private static function checkExpressionDoesNotContainDoubledOperators(string $stringExpression): void
	{
		if (preg_match('/[+\-\*\/]{2,}/', $stringExpression)) {
			throw new InvalidDoubledOperatorsException('The expression must not contain doubled operators.');
		}
	}

	private static function checkExpressionDoesNotEndWithOperator(string $stringExpression): void
	{
		$lastChar = substr($stringExpression, -1);

		if (OperatorRecognizer::isOperatorSymbol($lastChar)) {
			throw new ExpressionEndsWithOperatorException('The expression must not end with operator.');
		}
	}

	private static function checkElementBeforeClosingBracket(Expression $expression): void
	{
		if (!$expression->getLastElement() instanceof Number && !$expression->getLastElement() instanceof Expression) {
			throw new InvalidElementBeforeClosingBracketException('Closing brackets are allowed only after number and another closing bracket.');
		}
	}
}
