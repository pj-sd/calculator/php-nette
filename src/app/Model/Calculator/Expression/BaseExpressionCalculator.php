<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression;

use App\Model\Calculator\Exceptions\DivisionByZeroException;
use App\Model\Calculator\Exceptions\InvalidOperatorTypeException;
use App\Model\Calculator\Expression\Elements\Numerical\NumericalInterface;
use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\Division;
use App\Model\Calculator\Expression\Elements\Operators\Multiplication;
use App\Model\Calculator\Expression\Elements\Operators\OperatorInterface;
use App\Model\Calculator\Expression\Elements\Operators\Subtraction;

class BaseExpressionCalculator
{
	public static function calculateBaseExpression(
		NumericalInterface $firstNumber,
		OperatorInterface $operator,
		NumericalInterface $secondNumber
	): float
	{
		switch (true) {
			case $operator instanceof Addition:
				return self::runAddition($firstNumber, $secondNumber);
			case $operator instanceof Subtraction:
				return self::runSubtraction($firstNumber, $secondNumber);
			case $operator instanceof Multiplication:
				return self::runMultiplication($firstNumber, $secondNumber);
			case $operator instanceof Division:
				return self::runDivision($firstNumber, $secondNumber);
			default:
				throw new InvalidOperatorTypeException(sprintf('Unexpected operator type %s.', gettype($operator)));
		}
	}

	public static function runAddition(NumericalInterface $firstNumber, NumericalInterface $secondNumber): float
	{
		return $firstNumber->getValue() + $secondNumber->getValue();
	}

	public static function runSubtraction(NumericalInterface $firstNumber, NumericalInterface $secondNumber): float
	{
		return $firstNumber->getValue() - $secondNumber->getValue();
	}

	public static function runMultiplication(NumericalInterface $firstNumber, NumericalInterface $secondNumber): float
	{
		return $firstNumber->getValue() * $secondNumber->getValue();
	}

	public static function runDivision(NumericalInterface $firstNumber, NumericalInterface $secondNumber): float
	{
		$secondNumberValue = $secondNumber->getValue();

		if ($secondNumberValue === 0.0) {
			throw new DivisionByZeroException("It's not possible to divide by zero.");
		}

		return $firstNumber->getValue() / $secondNumberValue;
	}
}
