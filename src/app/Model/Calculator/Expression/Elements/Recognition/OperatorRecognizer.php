<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Recognition;

use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\Division;
use App\Model\Calculator\Expression\Elements\Operators\Multiplication;
use App\Model\Calculator\Expression\Elements\Operators\OperatorInterface;
use App\Model\Calculator\Expression\Elements\Operators\Subtraction;
use InvalidArgumentException;

class OperatorRecognizer
{
	private const OPERATOR_SYMBOLS = [
		Addition::SYMBOL,
		Subtraction::SYMBOL,
		Multiplication::SYMBOL,
		Division::SYMBOL,
	];

	public static function recognizeOperator(string $symbol): OperatorInterface
	{
		switch ($symbol) {
			case Addition::SYMBOL:
				return new Addition;
			case Subtraction::SYMBOL:
				return new Subtraction;
			case Multiplication::SYMBOL:
				return new Multiplication;
			case Division::SYMBOL:
				return new Division;
			default:
				throw new InvalidArgumentException(sprintf('Invalid operator symbol %s.', $symbol));
		}
	}

	public static function isOperatorSymbol(string $symbol): bool
	{
		return in_array($symbol, self::OPERATOR_SYMBOLS, true);
	}
}
