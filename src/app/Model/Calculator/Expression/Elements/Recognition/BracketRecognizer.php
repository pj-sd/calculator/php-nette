<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Recognition;

use App\Model\Calculator\Expression\Elements\Brackets\ClosingBracket;
use App\Model\Calculator\Expression\Elements\Brackets\OpeningBracket;

class BracketRecognizer
{
	public static function isOpeningBracketSymbol(string $symbol): bool
	{
		return $symbol === OpeningBracket::SYMBOL;
	}

	public static function isClosingBracketSymbol(string $symbol): bool
	{
		return $symbol === ClosingBracket::SYMBOL;
	}
}
