<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Recognition;

use Nette\Utils\Validators;

class NumberRecognizer
{
	public static function isDigit(int $value): bool
	{
		return $value >= 0 && $value <= 9;
	}

	public static function isCharDigit(string $char): bool
	{
		return Validators::isNumericInt($char) && self::isDigit((int) $char);
	}
}
