<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Brackets;

class ClosingBracket
{
	public const SYMBOL = ')';
}
