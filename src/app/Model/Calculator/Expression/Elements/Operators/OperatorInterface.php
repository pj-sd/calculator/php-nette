<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Operators;

use App\Model\Calculator\Expression\Elements\ElementInterface;

interface OperatorInterface extends ElementInterface
{

}
