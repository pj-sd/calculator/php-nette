<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Operators;

class Subtraction implements AdditionSubtractionInterface
{
	public const SYMBOL = '-';
}
