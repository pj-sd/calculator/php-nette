<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Numerical;

use App\Model\Calculator\Expression\Elements\ElementInterface;

interface NumericalInterface extends ElementInterface
{
	public function getValue(): float;
}
