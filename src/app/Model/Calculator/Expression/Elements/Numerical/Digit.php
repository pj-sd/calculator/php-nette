<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Numerical;

use App\Model\Calculator\Exceptions\InvalidArgumentException;
use App\Model\Calculator\Expression\Elements\Recognition\NumberRecognizer;

class Digit
{
	/** @var int */
	private $value;

	public function __construct(int $value)
	{
		$this->checkIsDigitValue($value);

		$this->value = $value;
	}

	public function getValue(): int
	{
		return $this->value;
	}

	private function checkIsDigitValue(int $value)
	{
		if (!NumberRecognizer::isDigit($value)) {
			throw new InvalidArgumentException(sprintf('Invalid digit %d.', $value));
		}
	}
}
