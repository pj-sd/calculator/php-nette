<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Numerical;

class Number implements NumericalInterface
{
	/** @var float */
	private $value;

	public function __construct(float $number)
	{
		$this->value = $number;
	}

	public function getValue(): float
	{
		return $this->value;
	}

	public function addDigit(Digit $digit)
	{
		$this->value = $this->value * 10 + $digit->getValue();
	}
}
