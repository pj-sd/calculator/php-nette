<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Expression\Elements\Numerical;

use App\Model\Calculator\Exceptions\InvalidExpressionPositionException;
use App\Model\Calculator\Exceptions\InvalidNumberPositionException;
use App\Model\Calculator\Exceptions\InvalidOperatorPositionException;
use App\Model\Calculator\Expression\Elements\ElementInterface;
use App\Model\Calculator\Expression\Elements\Operators\OperatorInterface;
use App\Model\Calculator\Expression\Elements\Operators\Subtraction;
use App\Model\Calculator\Expression\ExpressionCalculator;
use BadMethodCallException;

class Expression implements NumericalInterface
{
	/** @var Expression|null */
	private $parent;

	/** @var ElementInterface[] */
	private $elements = [];

	/** @var ElementInterface|null */
	private $lastElement;

	public function __construct(Expression $parent = null)
	{
		if ($parent instanceof self) {
			$parent->addExpression($this);
		}

		$this->parent = $parent;
	}

	public function hasParent(): bool
	{
		return $this->parent instanceof self;
	}

	public function getParent(): self
	{
		if (!$this->parent instanceof self) {
			throw new BadMethodCallException('Unable to call this method for expression without parent.');
		}

		return $this->parent;
	}

	/**
	 * @return ElementInterface[]
	 */
	public function getElements(): array
	{
		return $this->elements;
	}

	public function getLastElement(): ?ElementInterface
	{
		return $this->lastElement;
	}

	public function addDigit(Digit $digit)
	{
		if ($this->lastElement instanceof Number) {
			$this->lastElement->addDigit($digit);
			return;
		}

		if ($this->lastElement !== null && !$this->lastElement instanceof OperatorInterface) {
			throw new InvalidNumberPositionException('Number is allowed only at start of expression, after operator and after opening bracket.');
		}

		$number = new Number((float) $digit->getValue());
		$this->addElement($number);
	}

	public function addOperator(OperatorInterface $operator)
	{
		if (!($this->lastElement instanceof Number || $this->lastElement instanceof Expression)
			&& !($operator instanceof Subtraction && $this->lastElement === NULL)) {
			throw new InvalidOperatorPositionException('Operator is allowed only after integer and closing bracket.');
		}

		$this->addElement($operator);
	}

	public function addExpression(Expression $expression)
	{
		if ($this->lastElement !== null && !$this->lastElement instanceof OperatorInterface) {
			throw new InvalidExpressionPositionException('Opening bracket is allowed only at start of expression, after operator and after opening bracket.');
		}

		$this->addElement($expression);
	}

	public function getValue(): float
	{
		return ExpressionCalculator::calculateExpression($this);
	}

	private function addElement(ElementInterface $element)
	{
		$this->elements[] = $element;
		$this->lastElement = $element;
	}
}
