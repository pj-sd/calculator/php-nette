<?php
declare(strict_types = 1);

namespace App\Model\Calculator;

use App\Model\Calculator\Expression\Elements\Numerical\Expression;
use App\Model\Calculator\Expression\ExpressionParser;

class Calculator
{
	public static function calculateStringExpression(string $stringExpression): float
	{
		$expression = ExpressionParser::parseString($stringExpression);

		return self::calculateExpression($expression);
	}

	public static function calculateExpression(Expression $expression): float
	{
		return $expression->getValue();
	}
}
