<?php
declare(strict_types = 1);

namespace App\Model\Calculator\Exceptions;

interface CalculatorExceptionInterface extends \Throwable
{

}
