<?php
declare(strict_types = 1);

namespace App\Presenters;

use App\Components\Calculator\CalculatorControl;
use Nette\Application\UI\Presenter;

class HomepagePresenter extends Presenter
{
	/** @var string */
	private $expression = '';

	public function actionDefault(string $expression = ''): void
	{
		$this->expression = $expression;
	}

	protected function createComponentCalculator(): CalculatorControl
	{
		$calculator = new CalculatorControl();

		if ($this->expression) {
			$calculator->setDefaultExpression($this->expression);
		}

		return $calculator;
	}
}
