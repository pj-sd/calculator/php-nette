<?php
declare(strict_types = 1);

namespace App\Components\Calculator;

use App\Model\Calculator\Calculator;
use App\Model\Calculator\Exceptions\CalculatorExceptionInterface;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Bridges\ApplicationLatte\Template;

class CalculatorControl extends Control
{
	/** @var string */
	private $defaultExpression = '';

	/** @var float|null */
	private $calculatorResult;

	/** @var string|null */
	private $calculatorError;

	public function setDefaultExpression(string $defaultExpression): self
	{
		$this->defaultExpression = $defaultExpression;
		$this->calculateExpression($defaultExpression);

		return $this;
	}

	public function render()
	{
		/** @var Template $template */
		$template = $this->template;

		$template->setParameters([
			'calculatorResult' => $this->calculatorResult,
			'calculatorError' => $this->calculatorError,
		]);

		$template->render(__DIR__ . '/Calculator.latte');
	}

	protected function createComponentForm(): Form
	{
		$onSuccess = function (string $expression) {
			$this->calculateExpression($expression);
		};

		return CalculatorFormFactory::create($onSuccess, $this->defaultExpression);
	}

	private function calculateExpression(string $expression)
	{
		$this->calculatorResult = null;
		$this->calculatorError = null;

		try {
			$this->calculatorResult = Calculator::calculateStringExpression($expression);
		} catch (CalculatorExceptionInterface $e) {
			$this->calculatorError = $e->getMessage();
		}
	}
}
