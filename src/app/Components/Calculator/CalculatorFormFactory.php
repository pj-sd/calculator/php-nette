<?php
declare(strict_types = 1);

namespace App\Components\Calculator;

use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class CalculatorFormFactory
{
	const FIELD_EXPRESSION = 'expression';

	public static function create(callable $onSuccess, string $defaultExpression = ''): Form
	{
		$form = new Form();

		$form->addTextArea(self::FIELD_EXPRESSION, 'Expression')
			->setRequired('%label is required.')
			->addRule(Form::MAX_LENGTH, 'Max length %d characters exceeded.', 10000);

		$form->addSubmit('submit', 'Submit');

		$form->setDefaults([
			self::FIELD_EXPRESSION => $defaultExpression,
		]);

		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($onSuccess) {
			$onSuccess($values[self::FIELD_EXPRESSION]);
		};

		return $form;
	}
}
