<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator\Expression;

use App\Model\Calculator\Expression\ExpressionCleaner;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers ExpressionCleaner
 */
class ExpressionCleanerTest extends TestCase
{
	/**
	 * @test
	 * @covers ExpressionCleaner::clearStringExpression
	 */
	public function iWillGetCorrectlyClearedStringExpression(): void
	{
		$stringExpression = '5 + (11 + -20 * -5 - -23) / -(55 + 8)';
		$expectedStringExpression = '5+(11-20*(-1)*5+23)/(-1)/(55+8)';

		$clearedStringExpression = ExpressionCleaner::clearStringExpression($stringExpression);

		self::assertEquals($expectedStringExpression, $clearedStringExpression);
	}
}
