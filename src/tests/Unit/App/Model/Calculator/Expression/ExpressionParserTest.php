<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator\Expression;

use App\Model\Calculator\Exceptions\ExpressionEndsWithOperatorException;
use App\Model\Calculator\Exceptions\InvalidCharactersException;
use App\Model\Calculator\Exceptions\InvalidDoubledOperatorsException;
use App\Model\Calculator\Exceptions\InvalidElementBeforeClosingBracketException;
use App\Model\Calculator\Exceptions\MissingClosingBracketException;
use App\Model\Calculator\Exceptions\MissingOpeningBracketException;
use App\Model\Calculator\Expression\Elements\Numerical\Digit;
use App\Model\Calculator\Expression\Elements\Numerical\Expression;
use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\Division;
use App\Model\Calculator\Expression\Elements\Operators\Multiplication;
use App\Model\Calculator\Expression\Elements\Operators\Subtraction;
use App\Model\Calculator\Expression\ExpressionParser;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers ExpressionParser
 */
class ExpressionParserTest extends TestCase
{
	/**
	 * @test
	 * @covers ExpressionParser::parseString
	 */
	public function iWillGetCorrectlyParsedExpression(): void
	{
		$stringExpression = '5 + (11 + -20 * -5 - -23) / -(55 + 8)';
		$expectedExpression = $this->createExpectedExpression();

		$expression = ExpressionParser::parseString($stringExpression);

		self::assertEquals($expectedExpression, $expression);
	}

	/**
	 * @test
	 * @covers ExpressionParser::parseString
	 * @dataProvider iWillRetrieveInvalidArgumentExceptionProvider
	 *
	 * @param string $stringExpression
	 * @param string $expectedExceptionClassName
	 */
	public function iWillRetrieveInvalidArgumentException(
		string $stringExpression,
		string $expectedExceptionClassName
	): void
	{
		self::expectException($expectedExceptionClassName);

		ExpressionParser::parseString($stringExpression);
	}

	public function iWillRetrieveInvalidArgumentExceptionProvider(): array
	{
		return [
			'ExpressionEndsWithOperator' => ['15 + 44 *', ExpressionEndsWithOperatorException::class],
			'InvalidCharacters' => ['15 a 44', InvalidCharactersException::class],
			'InvalidDoubledOperators' => ['15 +* 44', InvalidDoubledOperatorsException::class],
			'InvalidElementBeforeClosingBracket' => ['(15 + 3 -)', InvalidElementBeforeClosingBracketException::class],
			'MissingClosingBracket' => ['(15 + 3', MissingClosingBracketException::class],
			'MissingOpeningBracket' => ['15 + 3)', MissingOpeningBracketException::class],
		];
	}

	/**
	 * Create expression for test iWillGetCorrectlyParsedExpression
	 *
	 * @return Expression
	 */
	private function createExpectedExpression(): Expression
	{
		$rootExpression = new Expression;
		$rootExpression->addDigit(new Digit(5));
		$rootExpression->addOperator(new Addition);

		$firstChildExpression = new Expression($rootExpression);
		$firstChildExpression->addDigit(new Digit(1));
		$firstChildExpression->addDigit(new Digit(1));
		$firstChildExpression->addOperator(new Subtraction);
		$firstChildExpression->addDigit(new Digit(2));
		$firstChildExpression->addDigit(new Digit(0));
		$firstChildExpression->addOperator(new Multiplication);

		$firstChildChildExpression = new Expression($firstChildExpression);
		$firstChildChildExpression->addOperator(new Subtraction);
		$firstChildChildExpression->addDigit(new Digit(1));

		$firstChildExpression->addOperator(new Multiplication);
		$firstChildExpression->addDigit(new Digit(5));
		$firstChildExpression->addOperator(new Addition);
		$firstChildExpression->addDigit(new Digit(2));
		$firstChildExpression->addDigit(new Digit(3));

		$rootExpression->addOperator(new Division);

		$secondChildExpression = new Expression($rootExpression);
		$secondChildExpression->addOperator(new Subtraction);
		$secondChildExpression->addDigit(new Digit(1));

		$rootExpression->addOperator(new Division);

		$thirdChildExpression = new Expression($rootExpression);
		$thirdChildExpression->addDigit(new Digit(5));
		$thirdChildExpression->addDigit(new Digit(5));
		$thirdChildExpression->addOperator(new Addition);
		$thirdChildExpression->addDigit(new Digit(8));

		return $rootExpression;
	}
}
