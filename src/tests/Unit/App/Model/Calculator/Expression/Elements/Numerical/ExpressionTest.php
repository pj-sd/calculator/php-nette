<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator\Expression\Elements\Numerical;

use App\Model\Calculator\Exceptions\InvalidExpressionPositionException;
use App\Model\Calculator\Exceptions\InvalidNumberPositionException;
use App\Model\Calculator\Exceptions\InvalidOperatorPositionException;
use App\Model\Calculator\Expression\Elements\Numerical\Digit;
use App\Model\Calculator\Expression\Elements\Numerical\Expression;
use App\Model\Calculator\Expression\Elements\Numerical\Number;
use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\Division;
use App\Model\Calculator\Expression\Elements\Operators\Multiplication;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers Expression
 */
class ExpressionTest extends TestCase
{
	/**
	 * @test
	 * @covers Expression::addDigit
	 */
	public function iWillAddDigitSuccessfully(): void
	{
		$expression = new Expression;
		$expression->addDigit(new Digit(1));
		$expression->addOperator(new Addition);
		$expression->addDigit(new Digit(5));
		$expression->addDigit(new Digit(2));

		$expectedElements = [
			new Number(1.0),
			new Addition,
			new Number(52.0),
		];

		self::assertEquals($expectedElements, $expression->getElements());
	}

	/**
	 * @test
	 * @covers Expression::addDigit
	 */
	public function iWillRetrieveInvalidNumberPositionException(): void
	{
		self::expectException(InvalidNumberPositionException::class);

		$expression = new Expression;
		$childExpression = new Expression($expression);
		$childExpression->addDigit(new Digit(1));
		$expression->addDigit(new Digit(1));
	}

	/**
	 * @test
	 * @covers Expression::addOperator
	 */
	public function iWillAddOperatorSuccessfully(): void
	{
		$expression = new Expression;
		$expression->addDigit(new Digit(1));
		$expression->addOperator(new Addition);
		$expression->addDigit(new Digit(5));
		$expression->addDigit(new Digit(2));
		$expression->addOperator(new Division);
		$childExpression = new Expression($expression);
		$childExpression->addDigit(new Digit(1));
		$expression->addOperator(new Multiplication);
		$expression->addDigit(new Digit(5));


		$expectedElements = [
			new Number(1.0),
			new Addition,
			new Number(52.0),
			new Division,
			$childExpression,
			new Multiplication,
			new Number(5.0),
		];

		self::assertEquals($expectedElements, $expression->getElements());
	}

	/**
	 * @test
	 * @covers Expression::addOperator
	 */
	public function iWillRetrieveInvalidOperatorPositionException(): void
	{
		self::expectException(InvalidOperatorPositionException::class);

		$expression = new Expression;
		$expression->addOperator(new Addition);
	}

	/**
	 * @test
	 * @covers Expression::addExpression
	 */
	public function iWillAddExpressionSuccessfully(): void
	{
		$expression = new Expression;
		$firstChildExpression = new Expression($expression);
		$firstChildExpression->addDigit(new Digit(1));
		$expression->addOperator(new Addition);
		$secondChildExpression = new Expression($expression);
		$secondChildExpression->addDigit(new Digit(1));

		$expectedElements = [
			$firstChildExpression,
			new Addition,
			$secondChildExpression,
		];

		self::assertEquals($expectedElements, $expression->getElements());
	}

	/**
	 * @test
	 * @covers Expression::addExpression
	 */
	public function iWillRetrieveInvalidExpressionPositionException(): void
	{
		self::expectException(InvalidExpressionPositionException::class);

		$expression = new Expression;
		$expression->addDigit(new Digit(1));
		$childExpression = new Expression($expression);
		$childExpression->addDigit(new Digit(1));
	}
}
