<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator\Expression;

use App\Model\Calculator\Exceptions\InvalidArgumentException;
use App\Model\Calculator\Expression\BaseExpressionCalculator;
use App\Model\Calculator\Expression\Elements\Numerical\Number;
use App\Model\Calculator\Expression\Elements\Operators\Addition;
use App\Model\Calculator\Expression\Elements\Operators\Division;
use App\Model\Calculator\Expression\Elements\Operators\Multiplication;
use App\Model\Calculator\Expression\Elements\Operators\OperatorInterface;
use App\Model\Calculator\Expression\Elements\Operators\Subtraction;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers BaseExpressionCalculator
 */
class BaseExpressionCalculatorTest extends TestCase
{
	private const FLOAT_ASSERTION_DELTA = 0.0001;

	/**
	 * @test
	 * @covers BaseExpressionCalculator::calculateBaseExpression
	 * @dataProvider iWillGetCorrectBaseExpressionResultProvider
	 *
	 * @param float $numberValue
	 * @param OperatorInterface $operator
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectBaseExpressionResult(
		float $numberValue,
		OperatorInterface $operator,
		float $expectedResult
	): void
	{
		$number = new Number($numberValue);
		$result = BaseExpressionCalculator::calculateBaseExpression($number, $operator, $number);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectBaseExpressionResultProvider(): array
	{
		return [
			'addition' => [5.0, new Addition, 10.0],
			'subtraction' => [5.0, new Subtraction, 0.0],
			'multiplication' => [5.0, new Multiplication, 25.0],
			'division' => [5.0, new Division, 1.0],
		];
	}

	/**
	 * @test
	 * @covers BaseExpressionCalculator::runAddition
	 * @dataProvider iWillGetCorrectAdditionResultProvider
	 *
	 * @param float $firstNumberValue
	 * @param float $secondNumberValue
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectAdditionResult(
		float $firstNumberValue,
		float $secondNumberValue,
		float $expectedResult
	): void
	{
		$result = BaseExpressionCalculator::runAddition(
			new Number($firstNumberValue),
			new Number($secondNumberValue)
		);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectAdditionResultProvider(): array
	{
		return [
			'positive + positive' => [15.0, 21.0, 36.0],
			'positive + negative' => [15.0, -12.0, 3.0],
			'negative + positive' => [-15.0, 12.0, -3.0],
			'negative + negative' => [-8.0, -33.0, -41.0],
			'zero + zero' => [0.0, 0.0, 0.0],
		];
	}

	/**
	 * @test
	 * @covers BaseExpressionCalculator::runSubtraction
	 * @dataProvider iWillGetCorrectSubtractionResultProvider
	 *
	 * @param float $firstNumberValue
	 * @param float $secondNumberValue
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectSubtractionResult(
		float $firstNumberValue,
		float $secondNumberValue,
		float $expectedResult
	): void
	{
		$result = BaseExpressionCalculator::runSubtraction(
			new Number($firstNumberValue),
			new Number($secondNumberValue)
		);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectSubtractionResultProvider(): array
	{
		return [
			'positive - positive' => [15.0, 21.0, -6.0],
			'positive - negative' => [15.0, -12.0, 27.0],
			'negative - positive' => [-15.0, 12.0, -27.0],
			'negative - negative' => [-8.0, -33.0, 25.0],
			'zero - zero' => [0.0, 0.0, 0.0],
		];
	}

	/**
	 * @test
	 * @covers BaseExpressionCalculator::runMultiplication
	 * @dataProvider iWillGetCorrectMultiplicationResultProvider
	 *
	 * @param float $firstNumberValue
	 * @param float $secondNumberValue
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectMultiplicationResult(
		float $firstNumberValue,
		float $secondNumberValue,
		float $expectedResult
	): void
	{
		$result = BaseExpressionCalculator::runMultiplication(
			new Number($firstNumberValue),
			new Number($secondNumberValue)
		);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectMultiplicationResultProvider(): array
	{
		return [
			'positive * positive' => [15.0, 21.0, 315.0],
			'positive * negative' => [15.0, -12.0, -180.0],
			'negative * positive' => [-15.0, 12.0, -180.0],
			'negative * negative' => [-8.0, -33.0, 264.0],
			'zero * zero' => [0.0, 0.0, 0.0],
		];
	}

	/**
	 * @test
	 * @covers BaseExpressionCalculator::runDivision
	 * @dataProvider iWillGetCorrectDivisionResultProvider
	 *
	 * @param float $firstNumberValue
	 * @param float $secondNumberValue
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectDivisionResult(
		float $firstNumberValue,
		float $secondNumberValue,
		float $expectedResult
	): void
	{
		$result = BaseExpressionCalculator::runDivision(
			new Number($firstNumberValue),
			new Number($secondNumberValue)
		);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectDivisionResultProvider(): array
	{
		return [
			'positive / positive' => [18.0, 9.0, 2.0],
			'positive / negative' => [44.0, -11.0, -4.0],
			'negative / positive' => [-44.0, 11.0, -4.0],
			'negative / negative' => [-126.0, -3.0, 42.0],
			'zero / positive' => [0.0, 23.0, 0.0],
		];
	}

	/**
	 * @test
	 * @covers BaseExpressionCalculator::runDivision
	 */
	public function iWillRetrieveDivideByZeroException(): void
	{
		self::expectException(InvalidArgumentException::class);

		BaseExpressionCalculator::runDivision(
			new Number(5.0),
			new Number(0.0)
		);
	}
}
