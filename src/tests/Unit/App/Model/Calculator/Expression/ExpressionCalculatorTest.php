<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator\Expression;

use App\Model\Calculator\Expression\ExpressionCalculator;
use App\Model\Calculator\Expression\ExpressionParser;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers ExpressionCalculator
 */
class ExpressionCalculatorTest extends TestCase
{
	/**
	 * @test
	 * @covers ExpressionCalculator::calculateExpression
	 * @dataProvider iWillGetCorrectExpressionResultProvider
	 *
	 * @param string $stringExpression
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectExpressionResult(string $stringExpression, float $expectedResult): void
	{
		$expression = ExpressionParser::parseString($stringExpression);
		$result = ExpressionCalculator::calculateExpression($expression);

		self::assertEquals($expectedResult, $result);
	}

	public function iWillGetCorrectExpressionResultProvider(): array
	{
		return [
			'multiplication/division only' => ['5 * 8 / 10', 4.0],
			'addition/subtraction only' => ['12 + 84 - 41', 55.0],
			'full calculation' => ['14 * (5 - 1) + 52 / 2', 82.0],
		];
	}
}
