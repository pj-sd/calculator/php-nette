<?php
declare(strict_types = 1);

namespace AppTest\Tests\Unit\App\Model\Calculator;

use App\Model\Calculator\Calculator;
use PHPUnit\Framework\TestCase;

/**
 * @group unit
 * @covers Calculator
 */
class CalculatorTest extends TestCase
{
	private const FLOAT_ASSERTION_DELTA = 0.0001;

	/**
	 * @test
	 * @covers Calculator::calculateStringExpression
	 * @covers Calculator::calculateExpression
	 * @dataProvider iWillGetCorrectResultProvider
	 *
	 * @param string $stringExpression
	 * @param float $expectedResult
	 */
	public function iWillGetCorrectResult(string $stringExpression, float $expectedResult): void
	{
		$result = Calculator::calculateStringExpression($stringExpression);

		self::assertEqualsWithDelta($expectedResult, $result, self::FLOAT_ASSERTION_DELTA);
	}

	public function iWillGetCorrectResultProvider(): array
	{
		return [
			'addition' => [
				'568 + 441',
				1009.0,
			],
			'subtraction' => [
				'9653 - 10247 - 21',
				-615.0,
			],
			'multiplication' => [
				'55 * 31 * -21',
				-35805.0,
			],
			'division' => [
				'-35805 / 55 / 31',
				-21.0,
			],
			'child expressions' => [
				'(55 + 31) / 6 * (88 + (631 - 24) * 2)',
				18662.0,
			],
		];
	}
}
