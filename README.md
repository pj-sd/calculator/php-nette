# Calculator / PHP + Nette

A simple app created by Patrik Jiráň & Štěpán Dalecký
just as a sample of their work in PHP and Nette Framework.


## Please notice...

- fully object-oriented approach in conformity with design patterns,
- error-preventing strictness,
- code transparency, comprehensibility, tidiness,
- unit tests coverage,
- code quality sustained by CI,
- very fast CI jobs.


### Comments

- Classes of calculator logic need neither configuration nor any services injection,
that's why they're made as static instead of using DI.


## Installation

The app is fully dockerized.


### Deployment

[GitLab CI](./.gitlab-ci.yml) automatically builds app image, tests it and deploys on
Kubernetes cluster after every merge into master.


### Development

To start the app incl. infrastructure with local src folder mounted into virtual server run

```bash
docker-compose up -d
```

and install dependencies running (don't forget to prefix by `winpty ` on Windows)

```bash
docker-compose exec app composer install
```

The app should be accessible by a browser on [localhost](http://localhost).


#### Debug Mode

Debug mode can be enabled by creating file `src/app/Config/.debug_mode`.
